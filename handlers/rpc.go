package handlers

import (
	"log"
	"net"
	"net/rpc"
)

func Listen() {
	rpc.Register(new(Watch))
	rpc.Register(new(Notification))

	l, err := net.Listen("tcp", cf.Addr)
	if err != nil {
		log.Fatal(err)
	}
	//@IMPORTANT do not use HttpServer or HttpListen for rpc
	//It will not work in docker container
	rpc.Accept(l)
}
