-- +migrate Up

ALTER TABLE ntf_watches
ADD FOREIGN KEY (entity) REFERENCES entities(name);

ALTER TABLE ntf_notifications
ADD FOREIGN KEY (entity) REFERENCES entities(name);

-- +migrate Down

ALTER TABLE ntf_watches
DROP CONSTRAINT ntf_watches_entity_fkey;

ALTER TABLE ntf_notifications
DROP CONSTRAINT ntf_notifications_entity_fkey;

