CREATE TABLE IF NOT EXISTS {schema}.{watch_table}(
	PRIMARY KEY (id),
	FOREIGN KEY (entity) REFERENCES public.entities(name)
) INHERITS (public.{watch_table});
