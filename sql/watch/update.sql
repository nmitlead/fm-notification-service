UPDATE {schema}.{table}
SET 
	entity=:entity,
	user_id=:user_id,
	actions=:actions,
	updated_at=current_timestamp at time zone 'utc',
	updated_by=:updated_by
WHERE id=:id AND deleted=false
RETURNING *
