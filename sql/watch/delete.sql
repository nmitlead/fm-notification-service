UPDATE {schema}.{table} SET deleted=true, deleted_at=current_timestamp at time zone 'utc', deleted_by=:deleted_by WHERE id=:id
