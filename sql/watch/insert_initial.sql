INSERT INTO {schema}.{table} (id, entity, actions, user_id, created_by, created_at) 
SELECT 
	'997a4059-2621-467c-af6b-caaef81426f9', 
	'issue', 
	{actions}, 
	'{user_id}', 
	'{created_by}', 
	now() 
WHERE NOT EXISTS (
	SELECT 1 
	FROM {schema}.{table} 
	WHERE id='997a4059-2621-467c-af6b-caaef81426f9'
);

INSERT INTO {schema}.{table} (id, entity, actions, user_id, created_by, created_at) 
SELECT 
	'e11a988e-e4b7-4a2e-9a39-0539034f8b2c', 
	'work', 
	{actions}, 
	'{user_id}', 
	'{created_by}', 
	now() 
WHERE NOT EXISTS (
	SELECT 1 
	FROM {schema}.{table} 
	WHERE id='e11a988e-e4b7-4a2e-9a39-0539034f8b2c'
);

INSERT INTO {schema}.{table} (id, entity, actions, user_id, created_by, created_at) 
SELECT 
	'71f8588c-9c0c-48cc-8e8e-3b2a81c79e12', 
	'vehicle', 
	{actions}, 
	'{user_id}', 
	'{created_by}', 
	now() 
WHERE NOT EXISTS (
	SELECT 1 
	FROM {schema}.{table} 
	WHERE id='71f8588c-9c0c-48cc-8e8e-3b2a81c79e12'
);
