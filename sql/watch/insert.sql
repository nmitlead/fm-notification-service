INSERT INTO {schema}.{table}
(
  id,
  entity,
  actions,
  user_id,
  created_at,
  created_by
)
VALUES
(
  :id,
  :entity,
  :actions,
  :user_id,
  current_timestamp at time zone 'utc',
  :created_by
) RETURNING *
