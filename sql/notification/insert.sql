INSERT INTO {table}
(
  id,
  content,
  user_id,
  fleet_id,
  entity,
  action,
  created_at,
  created_by
)
VALUES
(
  :id,
  :content,
  :user_id,
  :fleet_id,
  :entity,
  :action,
  current_timestamp at time zone 'utc',
  :created_by
) RETURNING *
