SELECT count(*) 
FROM {table} n
INNER JOIN {user_table} u ON n.created_by=u.id
WHERE n.deleted=false AND n.user_id=$1
