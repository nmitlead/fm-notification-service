UPDATE {table}
SET 
	viewed=:viewed,
	updated_at=current_timestamp at time zone 'utc',
	updated_by=:updated_by
WHERE id IN ({notification_ids}) AND deleted=false
RETURNING *
