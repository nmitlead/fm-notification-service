package models

import (
	"fm-libs/dbutil"
	"fm-libs/orm"
	"fm-libs/util"

	"fmt"
	"strconv"
)

const (
	ActionCreate = 1 << iota
	ActionUpdate
	ActionDelete
	ActionRestore
)

var (
	mapStrInt_actions = make(map[string]int)
)

func init() {
	mapStrInt_actions["created"] = ActionCreate
	mapStrInt_actions["updated"] = ActionUpdate
	mapStrInt_actions["deleted"] = ActionDelete
	mapStrInt_actions["restored"] = ActionRestore
}

type Watch struct {
	Id          string          `db:"id" json:"id"`
	Entity      string          `db:"entity" json:"entity"`
	Actions     int             `db:"actions"`
	UserId      string          `db:"user_id" json:"user_id"`
	ActionNames map[string]bool `db:"-" json:"actions"`

	orm.Std
}

func (m Watch) GetName() string {
	return "watch"
}

func (m Watch) GetTable() string {
	return "ntf_watches"
}

func (m *Watch) SetId(id string) {
	m.Id = id
}

//Orm helpers. Making models acting like active record
func (m *Watch) Create(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Insert(m, args, true)
}

func (m *Watch) Update(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Update(m, args)
}

func (m *Watch) Get(schema, id string) error {
	return ORM.Get(m, orm.ModelSqlFile(m, "getbypk"), orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}, id)
}

func (m *Watch) GetByUser(schema, user_id string, sort []string, page, pageSize int) ([]Watch, int, error) {
	dests := []Watch{}

	order := dbutil.OrderMap(map[string]string{
		"id": "id",
	}, sort, "id ASC")

	cnt, err := m.CountUser(schema, user_id)
	if err != nil {
		return nil, 0, err
	}

	offset, pages := util.GetPageOffset(page, pageSize, cnt)

	if offset == -1 {
		return nil, 0, ErrNotFound
	}

	if pages == -1 {
		return nil, 0, ErrPageNotFound
	}

	if pageSize < 1 {
		pageSize = 10
	}

	err = ORM.Select(&dests, orm.ModelSqlFile(m, "select_user"), orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}, user_id, order, pageSize, offset)

	if err != nil {
		return nil, 0, err
	}

	entities := m.GetWatchableEntities()
	for i, watch := range dests {

		entity := entities[watch.Entity]

		//zero value for map is nill
		//thats why map should be created manually
		dests[i].ActionNames = make(map[string]bool)
		for action, _ := range entity {
			if (watch.Actions & mapStrInt_actions[action]) != 0 {
				dests[i].ActionNames[action] = true
				continue
			}
			dests[i].ActionNames[action] = false
		}
	}

	return dests, pages, nil
}

func (m *Watch) Delete(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Delete(m, args)
}

func (m *Watch) Restore(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Restore(m, args)
}

func (m *Watch) CountUser(schema, user_id string) (int, error) {
	var count int
	args := orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}
	err := ORM.Get(&count, orm.ModelSqlFile(m, "count_user"), args, user_id)

	if err != nil {
		return 0, err
	}

	return count, nil
}

//convert action names to bitmask
func (m *Watch) ConvertActions(actions []string) (int, error) {
	var action_bits int
	for _, action := range actions {
		action_bit, ok := mapStrInt_actions[action]
		if !ok {
			return action_bits, fmt.Errorf("action not found: %s", action)
		}

		action_bits += action_bit
	}

	return action_bits, nil
}

//get watchers by entity name and action name
func (m *Watch) GetByMask(schema, entity, action string) ([]Watch, error) {
	var dests []Watch
	action_int, ok := mapStrInt_actions[action]
	if !ok {
		return dests, fmt.Errorf("unknown action: %s", action)
	}

	err := ORM.Select(&dests, orm.ModelSqlFile(m, "select_by_mask"), orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}, entity, action_int)

	return dests, err
}

func (m *Watch) GetWatchableEntities() map[string]map[string]bool {
	entities := make(map[string]map[string]bool)
	entities["work"] = map[string]bool{
		"created":  false,
		"updated":  false,
		"deleted":  false,
		"restored": false,
	}

	entities["issue"] = map[string]bool{
		"created":  false,
		"updated":  false,
		"deleted":  false,
		"restored": false,
	}

	entities["vehicle"] = map[string]bool{
		"created":  false,
		"updated":  false,
		"deleted":  false,
		"restored": false,
	}

	return entities
}

//isert inital watchers to watch table for user
func (m *Watch) InitialInsert(schema string) error {
	//setting max action bit mask
	m.Actions = 0
	for _, act := range mapStrInt_actions {
		m.Actions += act
	}

	args := orm.Args{
		"schema":     schema,
		"table":      m.GetTable(),
		"actions":    strconv.Itoa(m.Actions),
		"user_id":    m.UserId,
		"created_by": m.CreatedBy,
	}
	_, err := ORM.Exec("watch/insert_initial", args)
	return err
}
