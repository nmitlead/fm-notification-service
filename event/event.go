package event

import (
	"github.com/streadway/amqp"

	"fm-libs/config"
	"fm-libs/events"
	"fm-libs/log"
	pu "fm-libs/preputils"
	rbt "fm-libs/rabbit"
	"fm-libs/util"

	"notification/conf"
	"notification/logger"
	"notification/models"
	"notification/rabbit"

	"encoding/json"
	"fmt"
	"strings"
)

var (
	rmq *amqp.Connection
	cf  *conf.AppConf
)

type Contact struct {
	UserId string `json:"user_id"`
}

type ContactEvent struct {
	Data Contact `json:"data"`
	pu.EventData
}

const (
	QUEUE_NAME_ALL            = "notification-#"
	QUEUE_NAME_FLEET_CREATE   = "notification-fleet.fleet.created"
	QUEUE_NAME_CONTACT_CREATE = "notification-fleet.contact.created"
)

func initConf() error {
	//for not creating config in every call
	if cf != nil {
		return nil
	}

	var err error
	cf, err = conf.GetConf()

	if err != nil {
		fmt.Errorf("get config error: %s", err)
	}

	return nil
}

//Init event module
func Init() error {
	var err error

	if err := initConf(); err != nil {
		return err
	}

	err = logger.Init()
	if err != nil {
		return fmt.Errorf("init log error: %s", err)
	}

	rmq, err = rabbit.GetRabbit()
	if err != nil {
		return fmt.Errorf("get rabbit error: %s", err)
	}

	if err := models.Init(); err != nil {
		return fmt.Errorf("models init error: %s", err)
	}

	return nil
}

//Listen all events except fleet.fleet.created
//When event occurs get watchers waiting for notification to occured entity->action
//if watchers found in db create notifications for watchers
//if notifications created successfully return created notifications ids separated by comma(",") to success channel
//if any error occured while processing event return err to error channel
func ListenAllChanges() (<-chan map[string]interface{}, <-chan error) {
	errChan := make(chan error, 10)
	succChan := make(chan map[string]interface{}, 10)

	options := make(map[string]bool)
	queue := QUEUE_NAME_ALL
	if conf.RunMode == "test" {
		queue = ""
		options["queue_auto_delete"] = true
	}

	log.Log("listening rabbit event", log.InfoLevel, log.M{
		"event": "all",
	})

	_, err := rbt.Watch(rmq, queue, "#", rbt.ExchangeEvents, func(event amqp.Delivery) {
		var watch_model models.Watch

		//if event is fleet.created skip it
		if event.RoutingKey == cf.RoutingKeys.FleetCreate {
			return
		}

		var eventData pu.EventData
		if err := json.Unmarshal(event.Body, &eventData); err != nil {
			errChan <- err
			log.Log("event data unmarshall", log.ErrorLevel, log.M{
				"event": event.RoutingKey,
				"error": err.Error(),
				"body":  fmt.Sprintf("%s", event.Body),
			})
			return
		}

		fleet_id, ok := eventData.SessData[config.SessFleetKey]
		if !ok {
			errChan <- fmt.Errorf(
				"%s key not found in eventData.SessData map",
				config.SessFleetKey,
			)
			log.Log("sess key not found", log.ErrorLevel, log.M{
				"event": event.RoutingKey,
				"error": fmt.Sprintf(
					"%s key not found in eventData.SessData map",
					config.SessFleetKey,
				),
				"event_data": eventData,
			})
			return
		}

		schema := util.FleetSchema(fleet_id)

		//getting entity and action name from routing key
		//Ex: issue.label.created
		// service: "issue"
		// entity: "label"
		// action: "created"
		routing_keys := strings.Split(event.RoutingKey, ".")

		if len(routing_keys) < 3 {
			log.Log("invalid routing key", log.WarnLevel, log.M{
				"routing_key": event.RoutingKey,
			})
			return
		}

		entity := routing_keys[1]
		action := routing_keys[2]

		//getting watchers who watching entity and action
		watchers, err := watch_model.GetByMask(schema, entity, action)
		if err != nil {
			errChan <- err
			log.Log("get watchers by mask", log.ErrorLevel, log.M{
				"event":  event.RoutingKey,
				"error":  err.Error(),
				"schema": schema,
				"entity": entity,
				"action": action,
			})
			return
		}

		user_id, ok := eventData.SessData[config.SessUserIdKey]
		if !ok {
			errChan <- fmt.Errorf(
				"%s key not found in eventData.SessData map",
				config.SessUserIdKey,
			)
			log.Log("sess key not found", log.ErrorLevel, log.M{
				"error": fmt.Sprintf(
					"%s key not found in eventData.SessData map",
					config.SessUserIdKey,
				),
				"event":      event.RoutingKey,
				"event_data": eventData,
			})
			return
		}

		//@TODO dont send users notifications to himself

		//if watchers found in db create their notifications
		notifications_created := make([]string, len(watchers))
		user_ids := make([]string, 0)
		for _, watcher := range watchers {
			var notification_model models.Notification
			notification_model.UserId = watcher.UserId
			notification_model.Content = fmt.Sprintf("%s", event.Body)
			notification_model.FleetId = fleet_id
			notification_model.Entity = entity
			notification_model.Action = action
			notification_model.CreatedBy = user_id

			if err := notification_model.Create(); err != nil {
				errChan <- err
				log.Log("notification create", log.ErrorLevel, log.M{
					"event":  event.RoutingKey,
					"error":  err.Error(),
					"schema": schema,
					"model":  notification_model,
				})

				continue
			}

			user_ids = append(user_ids, watcher.UserId)
			notifications_created = append(notifications_created, notification_model.Id)
		}

		notify := events.Notify{
			UserIds:      user_ids,
			TemplateName: entity,
			Data:         map[string]interface{}{},
		}

		if err := rabbit.Emit(cf.RoutingKeys.Notify, notify); err != nil {
			errChan <- err
			log.Log("emit error", log.ErrorLevel, log.M{
				"event":  event.RoutingKey,
				"error":  err.Error(),
				"schema": schema,
				"notify": notify,
			})
			return
		}

		//Acknowledging rabbit
		if err := event.Ack(false); err != nil {
			log.Log("event acknowlegde", log.WarnLevel, log.M{
				"event": event.RoutingKey,
				"error": err.Error(),
			})

			errChan <- err
			return
		}

		//if watchers found in db and created succesfully send
		//created notifications ids separated by "," to success channel
		if len(notifications_created) > 0 {
			log.Log("notification created", log.InfoLevel, log.M{
				"notifications": notifications_created,
			})
			succChan <- map[string]interface{}{
				"fleet_id":         fleet_id,
				"notification_ids": notifications_created,
			}

		}
	}, options)

	if err != nil {
		errChan <- err
	}

	return succChan, errChan
}

//Listen for fleet create event and create new schema and insert initial data
//returns fleetId on success and errors on error
func ListenFleetCreate() (<-chan string, <-chan error) {
	errChan := make(chan error, 10)
	succChan := make(chan string, 10)

	options := make(map[string]bool)
	queue := QUEUE_NAME_FLEET_CREATE
	if conf.RunMode == "test" {
		queue = ""
		options["queue_auto_delete"] = true
	}

	log.Log("listening rabbit event", log.InfoLevel, log.M{
		"event": cf.RoutingKeys.FleetCreate,
	})

	_, err := rbt.Watch(rmq, queue, cf.RoutingKeys.FleetCreate, rbt.ExchangeEvents, func(event amqp.Delivery) {
		fmt.Println("SDSD")
		var eventData pu.FleetEventData

		if err := json.Unmarshal(event.Body, &eventData); err != nil {
			log.Log("event unmarshal", log.ErrorLevel, log.M{
				"event": cf.RoutingKeys.FleetCreate,
				"error": err.Error(),
			})

			errChan <- err
			return
		}

		fleet := eventData.Data

		//create new fleet schema
		fl_model := models.Fleet{}
		err := fl_model.Create(fleet.Id)
		if err != nil {
			log.Log("fleet create", log.ErrorLevel, log.M{
				"event": cf.RoutingKeys.FleetCreate,
				"error": err.Error(),
			})
			errChan <- err
			return
		}

		fl := map[string]interface{}{
			"id":         fleet.Id,
			"created_by": fleet.CreatedBy,
		}

		if err := rabbit.Emit(conf.ServiceName+".fleet.created", fl); err != nil {
			log.Log("event emit", log.ErrorLevel, log.M{
				"event": cf.RoutingKeys.FleetCreate,
				"error": err.Error(),
			})

			errChan <- err
			return
		}

		//Acknowledging rabbit
		if err := event.Ack(false); err != nil {
			log.Log("event acknowlegde", log.WarnLevel, log.M{
				"event": cf.RoutingKeys.FleetCreate,
				"error": err.Error(),
			})

			errChan <- err
			return
		}

		succChan <- fleet.Id
	}, options)

	if err != nil {
		errChan <- err
	}

	return succChan, errChan
}

func ListenContactCreate() (<-chan string, <-chan error) {
	errChan := make(chan error, 10)
	succChan := make(chan string, 10)

	queue := QUEUE_NAME_CONTACT_CREATE
	options := make(map[string]bool)
	if conf.RunMode == "test" {
		queue = ""
		options["queue_auto_delete"] = true
	}

	log.Log("listening rabbit event", log.InfoLevel, log.M{
		"event": cf.RoutingKeys.FleetCreate,
	})

	_, err := rbt.Watch(rmq, queue, cf.RoutingKeys.ContactCreate, rbt.ExchangeEvents, func(event amqp.Delivery) {
		var eventData ContactEvent

		if err := json.Unmarshal(event.Body, &eventData); err != nil {
			log.Log("event unmarshal", log.ErrorLevel, log.M{
				"event": cf.RoutingKeys.ContactCreate,
				"error": err.Error(),
			})

			errChan <- err
			return
		}

		contact := eventData.Data

		fleet_id, ok := eventData.SessData[config.SessFleetKey]
		if !ok {
			errChan <- fmt.Errorf(
				"%s key not found in eventData.SessData map",
				config.SessFleetKey,
			)
			log.Log("sess key not found", log.ErrorLevel, log.M{
				"event": event.RoutingKey,
				"error": fmt.Sprintf(
					"%s key not found in eventData.SessData map",
					config.SessFleetKey,
				),
				"event_data": eventData,
			})
			return
		}

		schema := util.FleetSchema(fleet_id)
		model := models.Watch{}
		model.UserId = contact.UserId
		model.CreatedBy = contact.UserId

		if err := model.InitialInsert(schema); err != nil {
			errChan <- err
			log.Log("insert inital watches", log.ErrorLevel, log.M{
				"event":      event.RoutingKey,
				"error":      err.Error(),
				"event_data": eventData,
			})
			return
		}

		//sending event acl.contact.created
		if err := rabbit.Emit(conf.ServiceName+".contact.created", contact); err != nil {
			log.Log("event emit", log.ErrorLevel, log.M{
				"event": cf.RoutingKeys.FleetCreate,
				"error": err.Error(),
			})

			errChan <- err
			return
		}

		//Acknowledging rabbit
		if err := event.Ack(false); err != nil {
			log.Log("event acknowlegde", log.WarnLevel, log.M{
				"event": cf.RoutingKeys.FleetCreate,
				"error": err.Error(),
			})

			errChan <- err
			return
		}

		succChan <- contact.UserId
	}, options)

	if err != nil {
		errChan <- err
	}

	return succChan, errChan
}
