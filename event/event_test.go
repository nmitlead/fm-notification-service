package event

import (
	"github.com/pborman/uuid"

	"fm-libs/config"
	pu "fm-libs/preputils"

	"notification/models"
	"notification/rabbit"

	"fmt"
	"testing"
	"time"
)

func CreateListenDeps() error {
	var model models.Watch
	model.Entity = "issue"
	model.UserId = UUID
	model.CreatedBy = UUID

	actions, err := model.ConvertActions([]string{"created"})
	if err != nil {
		return fmt.Errorf("convert actions error: %s", err)
	}

	model.Actions = actions

	if err := model.Create(schema); err != nil {
		fmt.Errorf("watch create error: %s", err)
	}

	return nil
}

func TestListenAll(t *testing.T) {
	if err := CreateListenDeps(); err != nil {
		t.Error(err)
		return
	}

	succChan, errChan := ListenAllChanges()

	if err := rabbit.Emit(
		"issue.issue.created",
		pu.EventData{
			Data: map[string]string{
				"id":         FID,
				"created_by": UUID,
			},
			SessData: map[string]string{
				config.SessFleetKey:  FID,
				config.SessUserIdKey: UUID,
			},
		},
	); err != nil {
		t.Error("publish error ", err)
	}

	for {
		select {
		case data := <-succChan:
			fleet_id, ok := data["fleet_id"]
			if ok && fleet_id == FID {
				t.Skip()
				return
			}
		case err := <-errChan:
			fmt.Println(err)
			return
		case <-time.After(10 * time.Second):
			t.Error("Event not emitted")
			return
		}
	}
}

func TestListenFleet(t *testing.T) {
	succChan, errChan := ListenFleetCreate()

	if err := rabbit.Emit(
		cf.RoutingKeys.FleetCreate,
		pu.FleetEventData{
			Data: pu.Fleet{
				Id:        FID,
				CreatedBy: UUID,
			},
		},
	); err != nil {
		t.Error("publish error ", err)
	}

	select {
	case err := <-errChan:
		if err != nil {
			t.Error("Listen error ", err)
		}
	case succ := <-succChan:
		fmt.Println("fleet create success: ", succ)
	case <-time.After(5 * time.Second):
		t.Error("Event not emitted")
	}

}

func TestListenContactCreate(t *testing.T) {
	succChan, errChan := ListenContactCreate()

	user_id := uuid.New()

	if err := rabbit.Emit(
		cf.RoutingKeys.ContactCreate,
		ContactEvent{
			Data: Contact{
				UserId: user_id,
			},
			EventData: pu.EventData{
				SessData: map[string]string{
					config.SessFleetKey: FID,
				},
			},
		},
	); err != nil {
		t.Error("publish error ", err)
	}

	select {
	case err := <-errChan:
		if err != nil {
			t.Error("Listen error ", err)
		}
	case succ := <-succChan:
		fmt.Println("contact create success: ", succ)
	case <-time.After(5 * time.Second):
		t.Error("Event not emitted")
	}

}
